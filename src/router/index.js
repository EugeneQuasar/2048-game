import { createRouter, createWebHistory } from 'vue-router'
import theGame from '../components/TheGame.vue'

const routes = [
    {
        path: '/',
        name: 'game',
        component: theGame,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router
